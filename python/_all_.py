#!/usr/bin/env python2

# system ----
import os
import sys
import numpy as np
import scipy.integrate as integrate
import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)

# local ----
import hernquist
import dehnen

class Profiles(object):

    def __init__(self, distribution='isotropic', **kwargs):
        self.hernquist = hernquist.Model(**kwargs)
        self.dehnen = dehnen.Model(**kwargs)
