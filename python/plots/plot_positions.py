#!/usr/bin/env python2

# system ----
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.patches import ConnectionPatch
from matplotlib import gridspec
import matplotlib.ticker as tic
import matplotlib.lines as mlines
import matplotlib.cm as cm

# local ----
sys.path.append('..')
import plot_parameters
import generate

N = int(1e+4)
hern_pos = np.zeros((N,3))
dehn_pos000 = np.zeros((N,3))
dehn_pos050 = np.zeros((N,3))
dehn_pos150 = np.zeros((N,3))
for _ in range(N):
    hern_pos[_] = generate.main(profile='hernquist').position()
    dehn_pos000[_] = generate.main(gamma=0.0,profile='dehnen').position()
    dehn_pos050[_] = generate.main(gamma=0.5,profile='dehnen').position()
    dehn_pos150[_] = generate.main(gamma=1.5,profile='dehnen').position()


import plot_parameters # Edit parameters in modules directory
fig = plt.figure(figsize=( 6/1.2, 6/1.2 ))
# Merging Y axis configuration
gs = gridspec.GridSpec(2,2, width_ratios=[1,1], height_ratios=[1,1])
gs.update(wspace=0.02, hspace=0.02)

ax1 = fig.add_subplot(gs[0])
ax2 = fig.add_subplot(gs[1], sharex=ax1,  sharey=ax1)
ax3 = fig.add_subplot(gs[2], sharex=ax1,  sharey=ax1)
ax4 = fig.add_subplot(gs[3], sharex=ax1,  sharey=ax1)

for _ in range(N):
    ax1.scatter(hern_pos[_][0], hern_pos[_][1], edgecolors='black', facecolors='None', alpha=0.1)
    ax2.scatter(dehn_pos000[_][0], dehn_pos000[_][1], edgecolors='black', facecolors='None', alpha=0.1)
    ax3.scatter(dehn_pos050[_][0], dehn_pos050[_][1], edgecolors='black', facecolors='None', alpha=0.1)
    ax4.scatter(dehn_pos150[_][0], dehn_pos150[_][1], edgecolors='black', facecolors='None', alpha=0.1)

circle1 = plt.Circle((0,0), 35, linestyle="-", facecolor="none", edgecolor='white', alpha=0.35, linewidth=2.5)
circle2 = plt.Circle((0,0), 35, linestyle="-", facecolor="none", edgecolor='indianred', alpha=0.35, linewidth=2.5)
circle3 = plt.Circle((0,0), 35, linestyle="-", facecolor="none", edgecolor='goldenrod', alpha=0.35, linewidth=2.5)
circle4 = plt.Circle((0,0), 35, linestyle="-", facecolor="none", edgecolor='c', alpha=0.35, linewidth=2.5)
ax1.add_artist(circle1)
ax2.add_artist(circle2)
ax3.add_artist(circle3)
ax4.add_artist(circle4)

ax1.set_xlim(-150,150)
ax1.set_ylim(-150,150)
plt.setp([ax1.get_xticklabels()], visible=False)
plt.setp([ax2.get_xticklabels()], visible=False)
plt.setp([ax3.get_xticklabels()], visible=False)
plt.setp([ax4.get_xticklabels()], visible=False)
plt.setp([ax1.get_yticklabels()], visible=False)
plt.setp([ax2.get_yticklabels()], visible=False)
plt.setp([ax3.get_yticklabels()], visible=False)
plt.setp([ax4.get_yticklabels()], visible=False)

fig.savefig('positions.png', format='png')

plt.show()



