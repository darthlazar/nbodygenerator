#!/usr/bin/env python2

# system ----
import matplotlib.pyplot as plt

params = {
        'axes.labelsize'    : 16,
        'axes.labelpad'     : 5,
        'axes.linewidth'    : 1.5,
        'backend'           : 'wxAgg',
        'figure.figsize'    : (6/1.2,6/1.2),
        'font.size'         : 15,
        'font.serif'        : 'Computer Modern',
        'font.weight'       : "bold",
        'font.weight'       : 'heavy',
        'legend.fontsize'   : 16,
        'legend.frameon'    : True,
        'lines.linewidth'   : 2.0,
        'lines.markersize'  : 2,
        'savefig.bbox'      : 'tight',
        'text.usetex'       : True,
        'xtick.labelsize'   : 13,
        'xtick.major.size'  : 6,
        'xtick.minor.size'  : 3.5,
        'xtick.major.width' : 1.5,
        'xtick.minor.width' : 1.5,
        'ytick.labelsize'   : 13,
        'ytick.major.size'  : 6,
        'ytick.minor.size'  : 3.5,
        'ytick.major.width' : 1.5,
        'ytick.minor.width' : 1.5,
        }
plt.rcParams.update(params)


