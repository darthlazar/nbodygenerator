#!/usr/bin/env python2

# system ----
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.patches import ConnectionPatch

# local ----
sys.path.append('..')
import plot_parameters
import hernquist
import dehnen


E = np.linspace(-120000, 0, 100)

q_hernq = hernquist.Isotropic().q_(E)
df_hernq = hernquist.Isotropic().df_q(q_hernq)

q_dehnen = dehnen.Isotropic().q_(E)
df_deh_000 = dehnen.Isotropic(gamma=0).df_q(q_dehnen)
df_deh_025 = dehnen.Isotropic(gamma=0.25).df_q(q_dehnen)
df_deh_050 = dehnen.Isotropic(gamma=0.50).df_q(q_dehnen)
df_deh_075 = dehnen.Isotropic(gamma=0.75).df_q(q_dehnen)
df_deh_125 = dehnen.Isotropic(gamma=1.25).df_q(q_dehnen)
df_deh_150 = dehnen.Isotropic(gamma=1.5).df_q(q_dehnen)

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(np.power(q_hernq,2),np.log10(df_hernq), c='black', ls='-', label=r'$\rm Hernquist$')

ax.plot(np.linspace(0,0), np.linspace(0,0), c='black', ls='--', label=r'$\rm Dehnen$')
ax.plot(q_dehnen, np.log10(df_deh_000), c='indianred',  ls="--", zorder=0)
ax.plot(q_dehnen, np.log10(df_deh_025), c='coral',ls='--', zorder=0)
ax.plot(q_dehnen, np.log10(df_deh_050), c='goldenrod', ls='--',zorder=0)
ax.plot(q_dehnen, np.log10(df_deh_075), c='forestgreen', ls='--',zorder=0)
ax.plot(q_dehnen, np.log10(df_deh_125), c='c', ls='--',zorder=0)
ax.plot(q_dehnen, np.log10(df_deh_150), c='cadetblue',ls='--', zorder=0)
leg1 = ax.legend(loc='upper left', fontsize=13, frameon=False)
ax.add_artist(leg1)

ax.set_xlim(0,1)
ax.set_ylim(-5,2)
ax.set_xlabel(r'$\mathcal{E} = - Ea/GM $')
ax.set_ylabel(r'$ \log_{10} f (E) $')

empty = Rectangle((0, 0), 0, 0, alpha=0.0)
leg = ax.legend([empty,empty,empty,empty,empty,empty],
        [r'$\gamma=0$', r'$\gamma = 1/4$', r'$\gamma = 1/2$',
         r'$\gamma = 3/4$', r'$\gamma = 5/4$', r'$\gamma = 3/2$'],
        loc="lower right", frameon=False, ncol=2, fontsize=13, handlelength=0, handletextpad=0)
leg.get_texts()[0].set_color('indianred')
leg.get_texts()[1].set_color('coral')
leg.get_texts()[2].set_color('goldenrod')
leg.get_texts()[3].set_color('forestgreen')
leg.get_texts()[4].set_color('c')
leg.get_texts()[5].set_color('cadetblue')


fig.savefig('df.png', format='png')

plt.show()
