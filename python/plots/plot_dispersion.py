#!/usr/bin/env python2

# system ----
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.patches import ConnectionPatch

# local ----
sys.path.append('..')
import plot_parameters
import hernquist
import dehnen

rad = 10**np.linspace(np.log10(1e-2), np.log10(1000), 100)

sig_hern = hernquist.Isotropic().squared_radial_dispersion(rad)
sig_deh_000 = dehnen.Isotropic(gamma=0).squared_radial_dispersion(rad)
sig_deh_025 = dehnen.Isotropic(gamma=0.25).squared_radial_dispersion(rad)
sig_deh_050 = dehnen.Isotropic(gamma=0.50).squared_radial_dispersion(rad)
sig_deh_075 = dehnen.Isotropic(gamma=0.75).squared_radial_dispersion(rad)
sig_deh_125 = dehnen.Isotropic(gamma=1.25).squared_radial_dispersion(rad)
sig_deh_150 = dehnen.Isotropic(gamma=1.5).squared_radial_dispersion(rad)


fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot(rad,np.sqrt(sig_hern), c='black', zorder=1)
ax.plot(rad,np.sqrt(sig_deh_000), c='indianred',  ls="--", zorder=0)
ax.plot(rad,np.sqrt(sig_deh_025), c='coral',ls='--', zorder=0)
ax.plot(rad,np.sqrt(sig_deh_050), c='goldenrod', ls='--',zorder=0)
ax.plot(rad,np.sqrt(sig_deh_075), c='forestgreen', ls='--',zorder=0)
ax.plot(rad,np.sqrt(sig_deh_125), c='c', ls='--',zorder=0)
ax.plot(rad,np.sqrt(sig_deh_150), c='cadetblue',ls='--', zorder=0)

ax.set_ylim(0,150)
ax.set_xscale('log')

ax.set_xlabel(r'$r\ [\rm kpc] $')
ax.set_ylabel(r'$ \sigma_{r}(r)\ [\rm km\ s^{-1}] $')

'''
empty = Rectangle((0, 0), 0, 0, alpha=0.0)
leg = ax.legend([empty,empty,empty,empty,empty,empty],
        [r'$\gamma=0$', r'$\gamma = 1/4$', r'$\gamma = 1/2$',
         r'$\gamma = 3/4$', r'$\gamma = 5/4$', r'$\gamma = 3/2$'],
        loc="lower center", frameon=False, ncol=3, fontsize=13, handlelength=0, handletextpad=0)
leg.get_texts()[0].set_color('indianred')
leg.get_texts()[1].set_color('coral')
leg.get_texts()[2].set_color('goldenrod')
leg.get_texts()[3].set_color('forestgreen')
leg.get_texts()[4].set_color('c')
leg.get_texts()[5].set_color('cadetblue')
'''

fig.savefig('disperse.png', format='png')

plt.show()

