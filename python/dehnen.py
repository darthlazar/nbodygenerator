#!/usr/bin/env python2

# system ----
import os
import sys
import numpy as np
import scipy.integrate as integrate
import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)

class Profile(object):

    def __init__(self, mass=1e12, scale_radius=35., gamma=1.5, **kwargs):
        self.M = mass # Msol
        self.a = scale_radius # kpc
        self.G = 4.3e-6 # km^2 s^-2 Msol^-1 kpc
        self.gamma = gamma

    def density(self, r):
        return (3 - self.gamma)/4. * self.M * (self.a/(np.power(r,self.gamma)*np.power(r+self.a,4.-self.gamma)))

    def potential(self,r):
        if(type(r) is np.ndarray):
            results = np.zeros(r.size, dtype='float')
            for i in range(0, r.size):
                results[i] = self.G * self.M / self.a
                if(0 < self.gamma < 2.):
                    results *= 1./(2.-self.gamma)
                elif(self.gamma == 2.):
                    results *= np.log(r[i]/(r[i]+self.a))
                elif(self.gamma > 2.):
                    results *= -(1./(2.-self.gamma)) * (1. - np.power(r[i]/r[i]+self.a,(2.-self.gamma)))
        else:
            results = self.G * self.M / self.a
            if(0 < self.gamma < 2.):
                results *= 1./(2.-self.gamma)
            elif(self.gamma == 2.):
                results *= np.log(r/(r+self.a))
            elif(self.gamma > 2.):
                results *= -(1./(2.-self.gamma)) * (1. - np.power(r/(r+self.a),(2.-self.gamma)))
        return results

    def mass_enclosed(self,r):
        return self.M * np.power(r/(r+self.a),(3.-self.gamma))

    def radius_from_mass(self,P):
        # P == M(<r)/Mtot
        return self.a * np.power(P,1./(3.-self.gamma))/(1. - np.power(P,1./(3.-self.gamma)))

    def escape_velocity(self, r):
	return np.sqrt(-2. * self.potential(r))


class Isotropic(Profile):

    def __init__(self, **kwargs):
        super(Isotropic, self).__init__(**kwargs)

    def squared_radial_dispersion(self, r):
	G = self.G
        if(type(r) is np.ndarray):
            results = np.zeros(r.size, dtype='float')
            for i in range(0, r.size):
                integrand = lambda x: (self.density(x) * self.G * self.mass_enclosed(x))/np.power(x,2)
                results[i] = 1./self.density(r[i]) * integrate.quad(integrand, r[i], np.inf, limit=100)[0]
        else:
            integrand = lambda x: (self.density(x) * self.G * self.mass_enclosed(x))/np.power(x,2)
            results = 1./self.density(r) * integrate.quad(integrand, r, np.inf, limit=100)[0]
	return results

    def df(self, rad, vel):
	E = self.energy(rad, vel)
	q = np.sqrt(-self.a*E/G/self.M)
	return self.df_q(q)

    def energy(self, rad, vel):
        return self.potential(rad) + np.power(vel,2)/2.

    def q_(self, E): # This quantity is squared compared to the Hernquist one
        return -self.a*E/(self.G*self.M)

    def psi_(self, phi):
        return -(phi*self.a)/(self.G*self.M)

    def y_psi(self, psi):
        if(type(psi) is np.ndarray):
            results = np.zeros(psi.size, dtype='float')
            for i in range(0, psi.size):
                if(self.gamma == 2.):
                    results[i] = np.exp(-psi[i])
                else:
                    results[i] = np.power((1.-(2.-self.gamma)*psi[i]),1./(2.-self.gamma))
        else:
            if(self.gamma == 2.):
                results = np.exp(-psi)
            else:
                results = np.power((1.-(2.-self.gamma)*psi),1./(2.-self.gamma))
        return results

    def df_q(self, q):
	M = self.M
	a = self.a
        G = self.G
        gamma = self.gamma
	factor = (3.-gamma)*M/(2.*np.power(2.*np.pi*np.pi*G*M*a,1.5))
        if(type(q) is np.ndarray):
            results = np.zeros(q.size, dtype='float')
            for i in range(0, q.size):
                integrand = lambda x: np.power(1.-self.y_psi(x),2) * (gamma + 2.*self.y_psi(x) + (4.-gamma)*np.power(self.y_psi(x),2)) / (np.power(self.y_psi(x),(4.-gamma)) * np.sqrt(q[i] - x))
                results[i] = factor * integrate.quad(integrand, 0.0, q[i], limit=100)[0]
        else:
            integrand = lambda x: np.power(1.-self.y_psi(x),2) * (gamma + 2.*self.y_psi(x) + (4.-gamma)*np.power(self.y_psi(x),2)) / (np.power(self.y_psi(x),(4.-gamma)) * np.sqrt(q - x))
            results = factor * integrate.quad(integrand, 0.0, q, limit=100)[0]
        return results

class Model(Profile):

    def __init__(self, **kwargs):
        super(Model, self).__init__(**kwargs)
        self.isotropic = Isotropic(**kwargs)


