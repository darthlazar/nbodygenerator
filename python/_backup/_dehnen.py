import os
import sys 

import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import scipy as sp
import scipy.integrate as integrate
from scipy.integrate import quad
from scipy import interpolate

import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning) 
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning) 

# CONSTANTS AND CONVERISONS
# -------------------------
'''
G = 6.67408e-11 # m^3 kg^-1 s^-2
conv1 = 6.640521e-29 # kpc^3 Msol^-1 s^-2
conv2 = 6.4e+4 # (km/s)^2 Msol^-1 kpc
'''
G = 4.3e-9 * 1e+3  # (km/s)^2 Msol^-1 kpc

class parameters(object):
	def __init__(self, M=1e+12, a=35., gamma=0., N=1e+6):
		self.M = float(M) # Total mass of halo in solar mass
		self.a = float(a) # Scale radius of profile in kpc
		self.gamma = float(gamma)
		self.N = float(N) # Total number of particles to generate

	def generate_nbody(self):
		print "Sit back, relax, and enjoy the show."
		N = int(self.N)
		pos = np.zeros((N,3))
		vel = np.zeros((N,3))
		for _ in range(N):
			r_vec = self.generate_pos()
			r_mag = np.sqrt(r_vec[0]*r_vec[0] + r_vec[1]*r_vec[1] + r_vec[2]*r_vec[2] )
			pos[_] += r_vec

			v_mag = self.generate_vel(r_mag)
			theta = np.random.uniform() * 2*np.pi
			varphi = np.random.uniform() * np.pi
			vel_x = v_mag[0] * np.sin(theta) * np.cos(varphi)
			vel_y = v_mag[0] * np.sin(theta) * np.sin(varphi)
			vel_z = v_mag[0] * np.cos(theta)
			v_vec = [vel_x, vel_y, vel_z]
			vel[_] += v_vec
		return [pos, vel]

	def generate_pos(self):
		a = self.a
		x = np.random.uniform() 
		if(x > 0):
			r = a * np.power(x, 1/(3-self.gamma))/(1-np.power(x, 1/(3-self.gamma)))
		elif(x == 0):
			r = 0
		theta = np.random.uniform() * 2*np.pi
		varphi = np.random.uniform() * np.pi
		pos_x = r * np.sin(theta) * np.cos(varphi)
		pos_y = r * np.sin(theta) * np.sin(varphi)
		pos_z = r * np.cos(theta)
		return [pos_x, pos_y, pos_z]

	def density(self, r):
		return (3 - self.gamma)*self.M/(4*np.pi) * self.a/np.power(r, self.gamma)/np.power(r+self.a, 4 - self.gamma)

	def potential(self, r):
		factor = G*self.M/self.a
		if(self.gamma == 2.):
			return factor * np.log(r/(r+self.a))
		else:
			return factor * -(1.-np.power(r/(r+self.a), 2-self.gamma))/(2-self.gamma)  

	def enclosed_mass(self, r):
		return self.M * np.power(r/(r+self.a), 3-self.gamma)

	def velocity_dispersion(self, rad):
		M = self.M
		a = self.a
		const = G*M*M*a/2/np.pi
		integrand = lambda x: np.power(x, 1-2*self.gamma)/np.power(x+a, 7-2*self.gamma)
		inte = [ const/self.density(i) * integrate.quad(integrand, i, np.inf, limit=2000)[0] for i in rad]
		return np.sqrt(inte)

	def yy(self, pot):
		#psi = - pot*self.a/G/self.M
		psi = pot
		if(self.gamma == 2.):
			return np.exp(-psi)
		else:
			return np.power( 1 - psi*(2-np.gamma), 1/(2-self.gamma))

	def df_q(self, q):
		M = self.M
		a = self.a
		gamma = selg.gamma
		constant = (3.-gamma)*M/2./np.power(2*np.pi*np.pi*G*M*a, 3./2)
		return [constant * integrate.quad(lambda x: (1-self.yy(x))**2. * (gamma + 2.*self.yy(x) + (4.-gamma)*self.yy(x))/np.power(self.yy(x), 4-gamma)/np.sqrt(q-x), 0, E, limit=2000)[0] for E in q]

	def distribution_function(self, rad, vel):
		E = self.potential(rad) + 0.5*vel*vel
		q = np.sqrt(-self.a*E/G/self.M)
		return self.df_q(q)

