import os
import sys 

import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import scipy as sp
import scipy.integrate as integrate
from scipy.integrate import quad
from scipy import interpolate

import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning) 
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning) 

# CONSTANTS AND CONVERISONS
# -------------------------
'''
G = 6.67408e-11 # m^3 kg^-1 s^-2
conv1 = 6.640521e-29 # kpc^3 Msol^-1 s^-2
conv2 = 6.4e+4 # (km/s)^2 Msol^-1 kpc
'''
G = 4.3e-9 * 1e+3  # (km/s)^2 Msol^-1 kpc

class parameters(object):
	def __init__(self, M=1e+12, a=35., N=1e+6):
		self.M = float(M) # Total mass of halo in solar mass
		self.a = float(a) # Scale radius of profile in kpc
		self.N = float(N) # Total number of particles to generate

	def generate_nbody(self):
		print "Sit back, relax, and enjoy the show."
		N = int(self.N)
		pos = np.zeros((N,3))
		vel = np.zeros((N,3))
		for _ in range(N):
			r_vec = self.generate_pos()
			r_mag = np.sqrt(r_vec[0]*r_vec[0] + r_vec[1]*r_vec[1] + r_vec[2]*r_vec[2] )
			pos[_] += r_vec

			v_mag = self.generate_vel(r_mag)
			theta = np.random.uniform() * 2*np.pi
			varphi = np.random.uniform() * np.pi
			vel_x = v_mag[0] * np.sin(theta) * np.cos(varphi)
			vel_y = v_mag[0] * np.sin(theta) * np.sin(varphi)
			vel_z = v_mag[0] * np.cos(theta)
			v_vec = [vel_x, vel_y, vel_z]
			vel[_] += v_vec
		return [pos, vel]

	def generate_pos(self):
		a = self.a
		x = np.random.uniform() 
		if(x > 0):
			r = a * np.sqrt(x)/(1-np.sqrt(x))
		elif(x == 0):
			r = 0
		theta = np.random.uniform() * 2*np.pi
		varphi = np.random.uniform() * np.pi
		pos_x = r * np.sin(theta) * np.cos(varphi)
		pos_y = r * np.sin(theta) * np.sin(varphi)
		pos_z = r * np.cos(theta)
		return [pos_x, pos_y, pos_z]

	def generate_vel(self, rad):
		pot = self.potential(rad)
		vmax = np.sqrt(-2. * pot)
		fmax = vmax * vmax * self.distribution_function(rad, vel=0)
		counter = 0.
		fail = 0.
		vel = []
		while(counter < 1.):
			wf = np.random.uniform() * fmax
			wv = np.random.uniform() * vmax
			if(wf <= (wv * wv * self.distribution_function(rad, vel=wv))):
				vel.append(wv)
				counter += 1.
			else:
				fail += 1.
		return vel

	def density(self, r):
		return self.M/2/np.pi * self.a/r/(self.a+r)**3.

	def enclosed_mass(self, r):
		return self.M * r*r/(r+self.a)/(r+self.a)

	def velocity_dispersion(self, rad):
		M = self.M
		a = self.a
		const = G*M*M*a/2/np.pi
		integrand = lambda x: 1/x/(x+a)**5.
		inte = [ const/self.density(i) * integrate.quad(integrand, i, np.inf, limit=2000)[0] for i in rad]
		return np.sqrt(inte)

	def potential(self, rad):
		return - G*self.M / (rad + self.a)

	def escape_velocity(self, rad):
		return np.sqrt(-2*self.potential(rad))

	def df_q(self, q):
		M = self.M
		a = self.a
		constant = (M/a/a/a)/4./np.pi/np.pi/np.pi/np.power( 2*G*M/a, 1.5 )
		return constant * (3. * np.arcsin(q) + q * np.sqrt(1.-q*q) * (1. - 2.*q*q) * (8 * q * q * q * q - 8 * q * q - 3))/np.power(1.-q*q, 2.5)

	def distribution_function(self, rad, vel):
		E = self.potential(rad) + 0.5*vel*vel
		q = np.sqrt(-self.a*E/G/self.M)
		return self.df_q(q)